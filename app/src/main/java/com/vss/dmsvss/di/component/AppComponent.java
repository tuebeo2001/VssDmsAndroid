package com.vss.dmsvss.di.component;

import android.app.Application;

import com.vss.dmsvss.VssApplication;
import com.vss.dmsvss.di.module.AppModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {

    void inject(VssApplication vssApplication);

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }
}
