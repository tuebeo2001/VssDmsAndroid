package com.vss.dmsvss.base;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewbinding.ViewBinding;

public abstract class BaseFragment<T extends ViewBinding> extends Fragment {
    protected T binding;

    protected abstract T getLayoutBinding();

    protected abstract void updateUI(@Nullable Bundle savedInstanceState);

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = getLayoutBinding();
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        updateUI(savedInstanceState);
    }

    protected void openFragment(int resId, Class fragmentClazz, Bundle args, boolean addBackstack) {
        String tag = fragmentClazz.getSimpleName();
        try {
            boolean isExisted = getChildFragmentManager().popBackStackImmediate(tag, 0);
            if (!isExisted) {
                Fragment fragment;
                try {
                    fragment = (Fragment) (fragmentClazz.asSubclass(Fragment.class)).newInstance();
                    fragment.setArguments(args);
                    FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                    transaction.add(resId, fragment, tag);
                    if (addBackstack) {
                        transaction.addToBackStack(tag);
                    }
                    transaction.commitAllowingStateLoss();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (java.lang.InstantiationException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void toast(String msg) {
        Toast.makeText(requireContext(), msg, Toast.LENGTH_SHORT).show();
    }
}
