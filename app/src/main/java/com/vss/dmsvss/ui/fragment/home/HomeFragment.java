package com.vss.dmsvss.ui.fragment.home;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.vss.dmsvss.R;
import com.vss.dmsvss.base.BaseFragment;
import com.vss.dmsvss.data.model.ModuleHome;
import com.vss.dmsvss.databinding.HomeFragmentBinding;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends BaseFragment<HomeFragmentBinding> {
    private AdapterHome adapterHome;
    private List<ModuleHome> listModuleHome = new ArrayList<>();
    private GridLayoutManager layoutManager;

    @Override
    protected HomeFragmentBinding getLayoutBinding() {
        return HomeFragmentBinding.inflate(getLayoutInflater());
    }

    @Override
    protected void updateUI(@Nullable Bundle savedInstanceState) {
        initData();
        initView();
    }

    private void initData() {
        listModuleHome.add(new ModuleHome(R.drawable.ic_list_route, getString(R.string.str_route_list)));
        listModuleHome.add(new ModuleHome(R.drawable.ic_customer, getString(R.string.str_customer)));
        listModuleHome.add(new ModuleHome(R.drawable.ic_product, getString(R.string.str_product)));
        listModuleHome.add(new ModuleHome(R.drawable.ic_report, getString(R.string.str_report)));
    }

    private void initView() {
        adapterHome = new AdapterHome(listModuleHome);
        layoutManager = new GridLayoutManager(requireContext(), 2);
        binding.rvHomeFrag.setAdapter(adapterHome);
        binding.rvHomeFrag.setLayoutManager(layoutManager);
    }
}
